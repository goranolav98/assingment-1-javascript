const laptopsElement = document.getElementById("laptops");
const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("loan")
const loanButtonElement = document.getElementById("loanButton");
const payElement = document.getElementById("pay");
const bankButtonElement = document.getElementById("bank");
const workButtonElement = document.getElementById("work");
const specsElement = document.getElementById("specs");
const displayElement= document.getElementById("display");
const descriptionElement = document.getElementById("description");
const priceElement = document.getElementById("price");
const buyButtonElement = document.getElementById("buy");
const repayLoanElement = document.getElementById("repayLoan");
const laptopImageElement = document.getElementById("laptopImage");

let laptops = [];
let balance = 200.0;
let pay = 0.00;
let numLoan = 0.00;
let loanSum = 0.00;


balanceElement.innerText = balance;





fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptops(laptops));

const addLaptops = (laptops) => {
    laptops.forEach(x => addLaptop(x));
    priceElement.innerText = laptops[0].price;
    specsElement.innerText = laptops[0].specs;
    displayElement.innerText = laptops[0].title;
    descriptionElement.innerText = laptops[0].description;
    laptopImageElement.src = "https://hickory-quilled-actress.glitch.me/" + laptops[0].image;
}
const addLaptop = (laptop) =>{
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
} 

const handleLaptopChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    priceElement.innerText = selectedLaptop.price; 
    specsElement.innerText = selectedLaptop.specs;
    displayElement.innerText = selectedLaptop.title;
    descriptionElement.innerText = selectedLaptop.description;
    laptopImageElement.src = "https://hickory-quilled-actress.glitch.me/" + selectedLaptop.image;
    
   
    
}
const workIncreasePay = () =>{
    pay += 100;
    payElement.innerText = pay;
}

const takeLoan = () =>{
    if (numLoan == 0){
        loanSum = prompt("Please enter desired loan: ");
        if(loanSum <= (balance * 2) ){
            loanElement.innerText = 'Your loan is: ' + loanSum;
        numLoan += parseFloat(1);
        balance += parseFloat(loanSum);
        balanceElement.innerText = balance;
        repayButton = document.createElement('button');
        repayButton.innerText = 'Repay loan';
        repayLoanElement.appendChild(repayButton);
        repayButton.addEventListener("click", repayLoan);
        }
        else{
            alert('Cant loan that much money')
        }  
    }
    else{
        alert('Cant have more loans') 
    }
}
const buylaptop = () =>{
    
    if(parseFloat(priceElement.innerText) <= balance){
        balance -= parseFloat(priceElement.innerText);
        balanceElement.innerText = balance;
        alert('Total change: ' + balance)

    }
    else{
        alert('Not enough money')
    }


}
const putMoneyInBank = () =>{
    if(numLoan>0){
        if((pay *0.10) > loanSum){
            const change = (pay *0.10) - loanSum;
            balance += (pay *0.90)+change;
            pay = 0.00;
            payElement.innerText = pay;
            balanceElement.innerText = balance;
            loanSum =0.00;
            loanElement.innerText = 'Your loan is: ' + loanSum;
            numLoan = 0;
            repayLoanElement.innerHTML="";
        }
        else{
            loanSum = loanSum - (pay *0.10);
            balance += (pay *0.90);
            pay = 0.00;
            payElement.innerText = pay;
            loanElement.innerText = 'Your loan is: ' + loanSum;
            balanceElement.innerText = balance;

        } 
    }
    else{
        balance += pay;
        pay = 0;
        payElement.innerText = pay;
        balanceElement.innerText = balance;
    }
}
const repayLoan = () =>{
    if(pay >= loanSum){
        const change = pay - loanSum;
        balance += change;
        pay = 0.00;
        payElement.innerText = pay;
        balanceElement.innerText = balance;
        loanSum = 0.00;
        loanElement.innerText = 'Your loan is: ' + loanSum;
        numLoan = 0;
        repayLoanElement.innerHTML="";
    }
    else{
        loanSum = loanSum - pay;
        pay = 0.00;
        payElement.innerText = pay;
        loanElement.innerText = 'Your loan is: ' + loanSum;

    }
    
}

laptopsElement.addEventListener("change", handleLaptopChange);
workButtonElement.addEventListener("click", workIncreasePay );
loanButtonElement.addEventListener("click",takeLoan);
buyButtonElement.addEventListener("click", buylaptop);
bankButtonElement.addEventListener("click", putMoneyInBank);

